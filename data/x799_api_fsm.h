/*******************************************************************************
Copyright (c) 2016 Heptagon
All rights reserved.
*******************************************************************************/

#ifndef _X799_API_FSM_H_
#define _X799_API_FSM_H_

#include <stdint.h>
#include <stddef.h>
#include "x799_device.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
#ifdef X799_EXPORTS
#define X799_API __declspec(dllexport)
#else
#define X799_API // for debug purpose, the main() function to compile to exe
#endif
#endif

#ifdef X799APIFSM_DEBUG
#define DEBUG_MODE  1
#else
#define DEBUG_MODE  0
#endif

// LIBSTATE
typedef int8_t LIB_STATE;

#define XFISH_STATE_START                    ((LIB_STATE)  0)
#define XFISH_STATE_FINISH                   ((LIB_STATE)  -1)
//#define XFISH_STATE_PROCE                    ((LIB_STATE)  2)
//#define XFISH_STATE_DUMMY                    ((LIB_STATE)  3)


// ERRORS
typedef int8_t X799_Error;

#define X799_ERROR_NONE                  ((X799_Error)     0)
#define X799_ERROR_INVALIDSTATE          ((X799_Error)    -1)
#define X799_ERROR_XTIME                 ((X799_Error)    -2)
#define X799_ERORR_INVALID_INT_TIME      ((X799_Error)    -3)
#define X799_ERORR_INVALID_GAIN          ((X799_Error)    -4)

// OPERATIONS, not only I2C
typedef uint8_t XFISH_OPERATION;
#define XFISH_OP_I2C_READ             ((XFISH_OPERATION)   0)
#define XFISH_OP_I2C_WRITE            ((XFISH_OPERATION)   1)
#define XFISH_OP_WAIT_FOR_INT         ((XFISH_OPERATION)   2)
#define XFISH_OP_NONE                 ((XFISH_OPERATION)   3)
#define XFISH_OP_PARSE                ((XFISH_OPERATION)   4)
#define XFISH_OP_ASSIGN               ((XFISH_OPERATION)   5)

// MaXimum I2C burst transfer buffer, hardware dependent value.
#define I2C_MAX_BUFFER 60 // as for IO_warrior: MaX bytes -> 62Bytes; USB-ISS -> 60Bytes.

#define XFISH_ADDR_NONE                 ((uint8_t)        0)
#define XFISH_SIZE_NONE                 ((uint8_t)        0)
#define XFISH_DATA_NONE                 ((uint8_t)        0)
#define XFISH_RETRY_NONE                ((uint8_t)        0)


typedef struct XFISH_FSM {
    int lib_state;
    uint8_t operation;
    uint8_t address;
    uint8_t size;
    uint8_t data_arr[I2C_MAX_BUFFER];
    struct XFISH_FSM *next;
} XFISH_FSM_t;

/** API SPECIFICATION major version */
#define X799_SPECIFICATION_VER_MAJOR   1
/** API SPECIFICATION minor version */
#define X799_SPECIFICATION_VER_MINOR   0
/** API SPECIFICATION patch version */
#define X799_SPECIFICATION_VER_PATCH   0

typedef struct {
    uint8_t      major;    /*!< major number */
    uint8_t      minor;    /*!< minor number */
    uint8_t      patch;    /*!< build number */
} X799_Version_t;




X799_API X799_Error X799_GetApiVersion(X799_Version_t *pVersion);

X799_API X799_Error X799_GetModuleID(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t data[4], uint32_t* pX799_SerialNumber);
X799_API X799_Error X799_DeivceID(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data , uint8_t* pDevID);
X799_API X799_Error X799_RevisionID(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint8_t* pRevID);


X799_API X799_Error X799_SetIntegrationTimeScalar(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint8_t XTime);
X799_API X799_Error X799_GetIntegrationTimeScalar(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint8_t* pXTime);

X799_API X799_Error X799_SetIntegrationTime(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, float  IntTime);
X799_API X799_Error X799_GetIntegrationTime(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, float* IintTime);


X799_API X799_Error X799_SetGain(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint16_t  Gain);
X799_API X799_Error X799_GetGain(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint16_t* Gain);

X799_API X799_Error X799_SetEnableRegister(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint8_t enable);

X799_API X799_Error X799_Get6ChADCdata(int8_t* lib_state,uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t data[12], uint16_t channel[6]);


#ifdef __cplusplus
}
#endif


#endif