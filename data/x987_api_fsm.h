/*******************************************************************************
Copyright (c) 2016 Heptagon
All rights reserved.
*******************************************************************************/

#ifndef _X987_API_FSM_H_
#define _X987_API_FSM_H_

#include <stdint.h>
#include <stddef.h>
#include "x987_device.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _WIN32
#ifdef X987_EXPORTS
#define X987_API __declspec(dllexport)
#else
#define X987_API // for debug purpose, the main() function to compile to exe
#endif
#endif

#ifdef X987APIFSM_DEBUG
#define DEBUG_MODE  1
#else
#define DEBUG_MODE  0
#endif

// LIBSTATE
typedef int8_t LIB_STATE;

#define XFISH_STATE_START                    ((LIB_STATE)  0)
#define XFISH_STATE_FINISH                   ((LIB_STATE)  -1)
//#define XFISH_STATE_PROCE                    ((LIB_STATE)  2)
//#define XFISH_STATE_DUMMY                    ((LIB_STATE)  3)


// ERRORS
typedef int8_t X987_Error;

#define X987_ERROR_NONE                  ((X987_Error)     0)
#define X987_ERROR_INVALIDSTATE          ((X987_Error)    -1)
#define X987_ERROR_ADDTIONAL_GAIN        ((X987_Error)    -2)
#define X987_ERORR_INVALID_GAIN          ((X987_Error)    -3)
#define X987_ERORR_INVALID_INT_TIME      ((X987_Error)    -4)


// OPERATIONS, not only I2C
typedef uint8_t XFISH_OPERATION;
#define XFISH_OP_I2C_READ             ((XFISH_OPERATION)   0)
#define XFISH_OP_I2C_WRITE            ((XFISH_OPERATION)   1)
#define XFISH_OP_WAIT_FOR_INT         ((XFISH_OPERATION)   2)
#define XFISH_OP_NONE                 ((XFISH_OPERATION)   3)
#define XFISH_OP_PARSE                ((XFISH_OPERATION)   4)
#define XFISH_OP_ASSIGN               ((XFISH_OPERATION)   5)

// MaXimum I2C burst transfer buffer, hardware dependent value.
#define I2C_MAX_BUFFER 60 // as for IO_warrior: MaX bytes -> 62Bytes; USB-ISS -> 60Bytes.

#define XFISH_ADDR_NONE                 ((uint8_t)        0)
#define XFISH_SIZE_NONE                 ((uint8_t)        0)
#define XFISH_DATA_NONE                 ((uint8_t)        0)
#define XFISH_RETRY_NONE                ((uint8_t)        0)

/** API SPECIFICATION major version */
#define X987_SPECIFICATION_VER_MAJOR   1
/** API SPECIFICATION minor version */
#define X987_SPECIFICATION_VER_MINOR   0
/** API SPECIFICATION patch version */
#define X987_SPECIFICATION_VER_PATCH   0

typedef struct {
    uint8_t      major;    /*!< major number */
    uint8_t      minor;    /*!< minor number */
    uint8_t      patch;    /*!< build number */
} X987_Version_t;




X987_API X987_Error X987_GetApiVersion(X987_Version_t *pVersion);

X987_API X987_Error X987_GetModuleID(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t data[5], uint32_t* pX987_ChipID);
X987_API X987_Error X987_DeivceID(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data , uint8_t* pDevID);
X987_API X987_Error X987_RevisionID(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint8_t* pRevID);


X987_API X987_Error X987_SetAdditionalGainCtrl(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t data[2], uint8_t ad_gain);
X987_API X987_Error X987_GetAdditionalGainCtrl(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t data[2], uint8_t* ad_gain);

X987_API X987_Error X987_SetIntegrationTime(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, float  IntTime);
X987_API X987_Error X987_GetIntegrationTime(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, float* pIntTime);


X987_API X987_Error X987_SetGain(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint16_t  Gain);
X987_API X987_Error X987_GetGain(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint16_t* Gain);

X987_API X987_Error X987_SetGain_debug(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data,
                                       uint16_t  Gain, uint8_t *pGain_idx, uint8_t *pGain2x, uint8_t *pAgain, uint8_t *pAls2x,
                                       uint8_t *data0, uint8_t *data1, uint16_t *pGain_input);

X987_API X987_Error X987_SetEnableRegister(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint8_t enable);

X987_API X987_Error X987_GetCRGBADCdata(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t data[8], uint16_t crgb[4]);

X987_API X987_Error X987_ClearInterrupt(int8_t* lib_state, uint8_t* operation, uint8_t* address, uint8_t* size, uint8_t* data, uint8_t ai_clear);

#ifdef __cplusplus
}
#endif


#endif